{
  "preset": "ts-jest",
  "testEnvironment": "node",
  "testPathIgnorePatterns": ["/node_modules/", "/.next/"],
  "moduleDirectories": ["node_modules", "src"]
}

