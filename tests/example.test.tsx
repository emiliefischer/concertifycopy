import React from 'react';
import Footer from '../Footer';
import { render, screen } from '@testing-library/react';

test('renders my component', () => {
  render(<Footer />);
  expect(screen.getByText('Hello, Footer!')).toBeInTheDocument();
});

