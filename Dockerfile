FROM node:18-alpine
RUN mkdir -p /src
WORKDIR /src
COPY . .
RUN npm install
EXPOSE 3000
CMD ["sh", "./entrypoint.sh"]


